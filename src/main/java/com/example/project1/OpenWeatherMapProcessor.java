package com.example.project1;
import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class OpenWeatherMapProcessor implements Processor {

    public static String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }

    public void process(Exchange exchange) throws Exception {
        String in = exchange.getIn().getBody(String.class);

        Map parsedJson = new Gson().fromJson(in, Map.class);

        // method fromJson turns {} into Maps, [] into Lists
        String longitude = ((Map)parsedJson.get("coord")).get("lon").toString();
        String latitude = ((Map)parsedJson.get("coord")).get("lat").toString();

        String sunr = ((Map)parsedJson.get("sys")).get("sunrise").toString();
        String suns = ((Map)parsedJson.get("sys")).get("sunset").toString();

        Map weather_section = (Map) (((List) parsedJson.get("weather")).get(0));
        String main_dsc = weather_section.get("main").toString();
        String additional_dsc = weather_section.get("description").toString();

        Map main_section = (Map)parsedJson.get("main");
        String temp = main_section.get("temp").toString();
        String temp_min = main_section.get("temp_min").toString();
        String temp_max = main_section.get("temp_max").toString();
        String humidity = main_section.get("humidity").toString();
        String pressure = main_section.get("pressure").toString();

        String country = ((Map)parsedJson.get("sys")).get("country").toString();
        String wind_speed = ((Map)parsedJson.get("wind")).get("speed").toString();
        String city = parsedJson.get("name").toString();
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        String ret = "\nWEATHER REPORT FOR " + date + ", " + city.toUpperCase() + ", " + country + "\n" +
                        "Coordinates:\t\tlon:" + longitude + ", lat:" + latitude + "\n" +
                        "Sunrise\\Sunset(ms):\t" + sunr + "\\" + suns + "\n" +
                        "Overall:\t\t" + main_dsc + " (" + additional_dsc + ")\n" +
                        "Temperature:\t\t" + temp + "C (min: " + temp_min + "C, max: " + temp_max + "C)\n" +
                        "Humidity:\t\t" + humidity + "\n" +
                        "Pressure:\t\t" + pressure + "hPa\n" +
                        "Wind speed:\t\t" + wind_speed + "km/h\n\n";

        exchange.getIn().setBody(ret);
    }
}