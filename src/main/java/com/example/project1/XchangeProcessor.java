package com.example.project1;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class XchangeProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        String in = exchange.getIn().getBody(String.class);
        String message = "Today's " + in.substring(in.indexOf("instrument=")+11, in.indexOf("/")) + "'s open price in dollars is: " + in.substring(in.indexOf("open=")+5, in.indexOf(", last="));
        message += "and close price: " + in.substring(in.indexOf("last=")+5, in.indexOf(", bid=")) + "\n";

        exchange.getIn().setBody(message);
    }
}