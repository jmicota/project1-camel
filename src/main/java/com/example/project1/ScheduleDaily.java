package com.example.project1;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;


@Component
public class ScheduleDaily extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        // scheduling for everyday at midnight (start and stop the weather route)

        String str = "0+0+0+?+*+*";
        from("quartz://myGroup/RepTimerStarter?cron=" + str)
                .to("direct:start");

        // stopping route 5 seconds later
        from("quartz://myGroup/RepTimerStopper?cron=5+0+0+?+*+*")
                .to("direct:stop");


        from("direct:start")
                .setBody (simple (""))
                .to("controlbus:route?routeId=WeatherRoute&action=start");

        from("direct:stop")
                .setBody (simple (""))
                .to("controlbus:route?routeId=WeatherRoute&action=stop");

        // route triggering once (starting automatically)
        from("weather:weatherInitial?location={{api.city}}&appid={{api.weather}}&" +
                "geolocationAccessKey=IPSTACK_ACCESS_KEY&geolocationRequestHostIP=LOCAL_IP&" +
                "units=METRIC&repeatCount=1&initialDelay=500")
                .routeId("WeatherRouteInitial")
                .process(new OpenWeatherMapProcessor())
                .to("direct:weather").to("direct:exchangeBTC").to("direct:exchangeDOGE");

        // route starting every midnight, stopped before end of safeDelay (10 seconds)
        from("weather:weatherRepetitive?location={{api.city}}&appid={{api.weather}}&" +
                "geolocationAccessKey=IPSTACK_ACCESS_KEY&geolocationRequestHostIP=LOCAL_IP&" +
                "units=METRIC&delay={{timer.safeDelay}}")
                .autoStartup(false)
                .routeId("WeatherRoute")
                .process(new OpenWeatherMapProcessor())
                .to("direct:weather").to("direct:exchangeBTC").to("direct:exchangeDOGE");


        from("direct:weather").to("file:C:/inputFolder?fileName=info.txt"); // &fileExist=Append


        from("direct:exchangeBTC").setBody(simple ("BTC/USDT")).to("direct:bitcoin");
        from("direct:bitcoin").to("xchange:binance?service=marketdata&method=ticker")
                .process(new XchangeProcessor())
                .to("direct:resultBTC");
        from("direct:resultBTC").to("file:C:/inputFolder?fileName=info.txt&fileExist=Append");

        from("direct:exchangeDOGE").setBody(simple ("DOGE/USDT")).to("direct:dogecoin");
        from("direct:dogecoin").to("xchange:binance?service=marketdata&method=ticker")
                .process(new XchangeProcessor())
                .to("direct:resultDOGE");
        from("direct:resultDOGE").to("file:C:/inputFolder?fileName=info.txt&fileExist=Append");


        from("file:C:/inputFolder?noop=true").delay(500).doTry().setHeader("subject", simple("Your daily brief"))
                .setHeader("to", simple("kmarkowski2000@outlook.com"))
                .to("smtps://smtp.gmail.com:465?username=yourdailyinformation@gmail.com&password=a1b2c3d4!");
    }


}
